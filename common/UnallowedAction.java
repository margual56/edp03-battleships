package common;

public class UnallowedAction extends Exception {
	private static final long serialVersionUID = -5634864553348079172L;

	public UnallowedAction() {
		super();
	}

	public UnallowedAction(String error_message) {
		super(error_message);
	}
}
