package common;

public class IncorrectlyPlacedShip extends Exception {
	private static final long serialVersionUID = -2993019501292356458L;

	public IncorrectlyPlacedShip() {
		super();
	}

	public IncorrectlyPlacedShip(String error_message) {
		super(error_message);
	}
}
