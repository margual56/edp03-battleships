package common;

public class InvalidShipSize extends Exception {
	private static final long serialVersionUID = -2993019501292356458L;

	public InvalidShipSize() {
		super();
	}

	public InvalidShipSize(String error_message) {
		super(error_message);
	}
}
