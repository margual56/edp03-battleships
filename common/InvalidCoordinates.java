package common;

public class InvalidCoordinates extends Exception {
	private static final long serialVersionUID = -2993019501292356458L;

	public InvalidCoordinates() {
		super();
	}

	public InvalidCoordinates(String error_message) {
		super(error_message);
	}
}
