package common;

import java.util.LinkedList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Service interface.
 */
public interface Battleship extends lib.DefaultService {
	/**
	 * Grids dimension.
	 */
	public final static int DIMENSION = 10;

	/**
	 * List with the ships sizes to be placed on the ocean.
	 */
	public final static List<Integer> SHIPS =
			// Version for trials
			Stream.of(3, 2).collect(Collectors.toCollection(LinkedList::new));
//			// Popular version
//			Stream.of(4, 3, 3, 2, 2, 2, 1, 1, 1, 1).collect(Collectors.toCollection(LinkedList::new));
//			// MB electronic version of 1989
//			Stream.of(5, 4, 3, 3, 2).collect(Collectors.toCollection(LinkedList::new));
//			// MB version on paper 1938
//			Stream.of(5, 4, 3, 2, 2, 1, 1).collect(Collectors.toCollection(LinkedList::new));

	/**
	 * State of the OOS when the game ends.
	 */
	public final static int GAME_END = 10;
	
	/**
	 * The representation as a string of characters of the ocean grid.
	 * @return the string of characters representing the ocean grid
	 */
	String oceanGrid();
	
	/**
	 * The representation as a string of characters of the shots grid.
	 * @return the string of characters representing the shots grid
	 */
	String shotsGrid();
	
	/**
	 * Returns the list of ships sizes that are left to be placed on the ocean grid.
	 * @return the list of ships sizes that are left to be placed
	 * @throws UnallowedAction if the game state is not the initial one
	 */
	List<Integer> shipsToBePlaced() throws UnallowedAction;
	
	/**
	 * Place a ship in the specified coordinates of the ocean grid
	 * @param str the string of characters with the initial and final coordinates of the ship
	 * @throws IllegalArgumentException if the format of the coordinates is incorrect
	 * @throws InvalidCoordinates if the given coordinates are outside the grid
	 * @throws BarcoMalPosicionado if the ship is adjacent to another one (with no water in between)
	 * @throws TamanioBarcoNoValido if the ship size is not correct
	 * @throws UnallowedAction if the game state is not the initial one
	 */
	void placeShip(String str)
			throws InvalidCoordinates, IncorrectlyPlacedShip, InvalidShipSize, UnallowedAction;
	
	/**
	 * Returns {@code true} if the player has an opponent or can be assigned to one,
	 * so that the game can start.
	 * @return {@code true} if there are two paired players and, therefore,
	 * the game can start.
	 * @throws UnallowedAction if the game has already started
	 */
	boolean startGame() throws UnallowedAction;
	
	/**
	 * Returns 0, 1 or {@code GAME_END}, if it is not the player's turn,
	 * if it is, or if the end of the game is reached, respectively.
	 * @return 0, 1, o {@code GAME_END}
	 * @throws UnallowedAction if the game is not started
	 */
	int turn() throws UnallowedAction;
	
	/**
	 * Coordinates of the opponent's ocean grid that is being attacked.
	 * @param shot the coordinates
	 * @return {@code "Miss"}, {@code "Hit"} or {@code "Sunk"} if the shot was
	 * unsuccessful, a ship was reached or sunk, respectively
	 * @throws UnallowedAction if the game is not started, or it is finished,
	 * or if it is not the player's turn
	 * @throws InvalidCoordinates if the given coordinates are incorrect or
	 * outside the grid
	 */
	String shotCoordinates(String shot)
			throws UnallowedAction, InvalidCoordinates;
	
	/**
	 * Returns the number of ships on the ocean that are not sunk.
	 * @return number of ships that are not sunk
	 */
	int numShipsOnOcean();
	
}
