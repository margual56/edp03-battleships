package client;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import common.IncorrectlyPlacedShip;
import common.InvalidShipSize;
import common.Battleship;
import common.InvalidCoordinates;
import common.UnallowedAction;
import lib.ChannelException;
import lib.CommClient;
import lib.Menu;
import lib.ProtocolMessages;
import lib.UnknownOperation;

public class Client {

	/**
	 * Clilent's communication channel. The communication with the server is
	 * established when this object is created.
	 */
	private static CommClient com; // client's communication channel

	/**
	 * Textual menu for launching events.
	 */
	private static Menu m; // interface

	private static void showGrid(String id) throws IOException, ChannelException {
		// create message to be sent
		ProtocolMessages request = new ProtocolMessages(id);
		// send message
		com.sendEvent(request);
		try {
			// wait for the response
			ProtocolMessages response = com.waitReply();
			// process response or exception
			System.out.println(com.processReply(response));
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Received from server: %s\n", e.getMessage());
		} catch (UnallowedAction | InvalidCoordinates e) {
			System.err.printf("Error: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (Exception e) {
			System.err.printf("%s: %s\n", e.getClass().getSimpleName(), e.getMessage());
		}
	}

	@SuppressWarnings("unchecked")
	private static Set<Integer> shipsToPlace() throws IOException, ChannelException {
		Set<Integer> sizes = new TreeSet<>();
		// create message to be sent
		ProtocolMessages request = new ProtocolMessages("shipsToBePlaced");
		// send message
		com.sendEvent(request);
		try {
			// wait for the response
			ProtocolMessages response = com.waitReply();
			// process response or exception
			List<Integer> left = (List<Integer>) com.processReply(response);
			left.forEach(n -> sizes.add(n));
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Recceived from server: %s\n", e.getMessage());
		} catch (UnallowedAction e) {
			System.err.printf("Unallowed action: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (Exception e) {
			System.err.printf("%s: %s\n", e.getClass().getSimpleName(), e.getMessage());
		}

		return sizes;
	}

	private static void placeShips() throws IOException, ChannelException {
		// ask for the sizes of the pending ships to be places
		// on the ocean
		Set<Integer> ships = shipsToPlace();
		int size;
		while (!ships.isEmpty()) { // there are still some ships left
			// shows the ocean
			showGrid("oceanGrid");

			// presents a menu with the sizes of the pending ships
			Menu sizes = new Menu("\nSizes of the ships", "Option? ", false);
			if (ships.size() > 1) {
				ships.forEach(n -> sizes.add(String.format("Ship of size %d", n), n));
				size = sizes.getInteger();
			} else { // only one size left (but more than one ship might be left)
				size = ships.iterator().next();
			}
			// ask for the coordinates of the edges of the ship
			System.out.printf("Coordinates of the edges of the ship with size %d? ", size);
			// create message to be sent
			ProtocolMessages request = new ProtocolMessages("placeShip", sizes.input().nextLine());
			// send message
			com.sendEvent(request);
			try {
				// wait for the response
				ProtocolMessages response = com.waitReply();
				// process response or exception
				com.processReply(response);
			} catch (ClassNotFoundException | UnknownOperation e) {
				System.err.printf("Received from server: %s\n", e.getMessage());
			} catch (InvalidCoordinates | IncorrectlyPlacedShip | InvalidShipSize e) {
				System.err.printf("Error: %s\n", e.getMessage());
			} catch (UnallowedAction e) {
				System.err.printf("Unallowed action: %s\n", e.getMessage());
			} catch (Exception e) {
				System.err.printf("%s: %s\n", e.getClass().getSimpleName(), e.getMessage());
			} finally {
				ships = shipsToPlace();
			}
		}

		// shows the ocean
		showGrid("oceanGrid");
	}

	private static void clientInterface() {
		m = new Menu("\nBattleship", "Option? ");

		m.add("Number of ships on the ocean.", () -> {
			System.out.printf("Ships on the ocean: %d\n\n", numShips());
		});
		m.add("Shoot at the opponent's ocean.", () -> shoot());
	}

	public static void main(String[] args) {

		try {
			// Establish the communication with the server
			// create the communication channel and establish the
			// connection with the service in localhost by default
			com = new CommClient();
			// activate the client's messages register
			com.activateMessageLog(); // optional
		} catch (UnknownHostException e) {
			System.err.printf("Unknown server. %s\n", e.getMessage());
			System.exit(-1); // exits with an error
		} catch (IOException | ChannelException e) {
			System.err.printf("Error: %s\n", e.getMessage());
			System.exit(-1); // exits with an error
		}

		try {
			// place the ships on the ocean
			placeShips();

			// if it is possible (opponent available), start the game
			while (!startingGame()) {
				System.out.println("Waiting for an opponent");
				Thread.sleep(5000);
			}

			// create the interface (menu for launching events)
			clientInterface();

			// launch events through the interface
			int n;
			do {
				// ask the service if it is my turn
				n = myTurn();
				
				while (n == 0 && n != Battleship.GAME_END) { // it is not my turn
					// show the shots grid
					showGrid("shotsGrid");

					while (n == 0) { // waiting for the turn
						Thread.sleep(3000);
						n = myTurn();

						// show the ocean
						showGrid("oceanGrid");
					}

					// show the ocean
					showGrid("oceanGrid");
				}

				// show the shots grid
				showGrid("shotsGrid");
			} while (n != Battleship.GAME_END && m.runSelection());

			if (n != Battleship.GAME_END) { // we resign the game
				System.out.printf("\nYou have resigned!\n");
			} else { // the game is finished
				System.out.printf("\nGame ended: %s\n", numShips() == 0 ? "you lost!" : "you won!");
			}
		} catch (IOException | ChannelException e) {
			System.err.printf("Error: %s\n", e.getMessage());
		} catch (Exception e) { // exception of the service
			System.err.printf("Error: %s\n", e.getMessage());
		} finally {
			// close the interface entry
			m.close();
			// close the communication channel and
			// disconnect the client
			com.disconnect();
		}

	} // main

	private static Object shoot() throws IOException, ChannelException {
		// TODO Call `shootCoordinates`
		
		Object objResponse = null;
	
		System.out.print("\nCoordinates? ");
		// create message to be sent
		ProtocolMessages request = new ProtocolMessages("shotCoordinates", m.input().nextLine());

		// send message
		com.sendEvent(request);

		// wait for the response
		ProtocolMessages response;
		try {
			response = com.waitReply();
			// process response or exception
			objResponse = com.processReply(response);
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Received from server: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (InvalidCoordinates e) {
			System.err.printf("The coordinates are invalid\n");
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return objResponse;
	}

	/**
	 * @return number of ships on the ocean
	 * @throws ChannelException
	 * @throws IOException
	 */
	private static int numShips() throws IOException, ChannelException {
		int nShips = -1;

		// create message to be sent
		ProtocolMessages request = new ProtocolMessages("numShipsOnOcean");

		// send message
		com.sendEvent(request);

		// wait for the response
		ProtocolMessages response;
		try {
			response = com.waitReply();
			// process response or exception
			nShips = (int) com.processReply(response);
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Received from server: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return nShips;
	}

	private static int myTurn() throws IOException, ChannelException {
		int state = -1;

		// create message to be sent
		ProtocolMessages request = new ProtocolMessages("turn");

		// send message
		com.sendEvent(request);

		// wait for the response
		ProtocolMessages response;
		try {
			response = com.waitReply();
			// process response or exception
			state = (int) com.processReply(response);
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Received from server: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return state;
	}

	private static boolean startingGame() throws IOException, ChannelException {
		// Call `startGame`, return false if game did not start
		boolean start = false;

		// create message to be sent
		ProtocolMessages request = new ProtocolMessages("startGame");

		// send message
		com.sendEvent(request);

		// wait for the response
		ProtocolMessages response;
		try {
			response = com.waitReply();
			// process response or exception
			start = (boolean) com.processReply(response);
		} catch (ClassNotFoundException | UnknownOperation e) {
			System.err.printf("Received from server: %s\n", e.getMessage());
		} catch (IOException | ChannelException e) {
			throw e;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return start;
	}

}
