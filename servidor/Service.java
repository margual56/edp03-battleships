package servidor;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import common.Battleship;
import common.IncorrectlyPlacedShip;
import common.InvalidCoordinates;
import common.InvalidShipSize;
import common.UnallowedAction;

public class Service implements Battleship {
	private final static String PATTERN_COORD = "^(\\p{Alpha}\\d{1,2}\\s*){2}$";

	// Common information for every OOS

	// players connected to the service and waiting to play
	// (the player must have arranged their ships)
	private volatile static List<Integer> playersWaiting = new LinkedList<>();
	// grids with the arrangements of the ships of each player
	// (the client's identifier is used as a key)
	private volatile static Map<Integer, Grid> playersOcean = new HashMap<Integer, Grid>();
	// opponents playing (key and value, client's identifier)
	private volatile static Map<Integer, Integer> opponent = new HashMap<>();
	// to indicate if it is the player's turn (false by default)
	private volatile static Map<Integer, Boolean> playerTurn = new HashMap<>();
	// number of ships not sunk on each player's ocean
	private volatile static Map<Integer, Integer> shipsOnOcean = new HashMap<>();

	// Object for the mutual exclusion when starting the game
	private volatile static Object mutex = new Object();
	// Object for the mutuaal exclusion when switching turns
	private volatile static Map<Integer, Object> mutexTurn = new HashMap<>();

	// Exclusive information for each OOS
	private int idClient; // identifier
	private List<Integer> shipsLeft; // sizes of the ships left to be placed on the grid
	private Grid ocean; // own grid with the ships
	private Grid shots; // opponent's grid, initially blank
	private int state; // state of the game

	Service(int id) {
		this.idClient = id;
		this.shipsLeft = new LinkedList<>(Battleship.SHIPS);
		this.ocean = new Grid();
		this.shots = new Grid();
		this.state = 0;

		playersOcean.put(this.idClient, ocean);
		playerTurn.put(this.idClient, false);
		shipsOnOcean.put(this.idClient, 0);
	}

	/**
	 * Returns the coordinates provided by the given string.
	 * 
	 * @param str representation as a string of characters of the coordinates
	 * @return the coordinates
	 * @throws InvalidCoordinates if the format of the string in not correct or the
	 *                            given position is outside the grid
	 */
	private Pair<Integer, Integer> position(String str) throws InvalidCoordinates {

		if (str == null || str.isEmpty() || !str.matches("^\\p{Alpha}\\d{1,2}$")) {
			throw new InvalidCoordinates("Coordeninates pattern: ^\\p{Alpha}\\d{1,2}$ (" + str + " given)");
		}

		int row = str.toUpperCase().charAt(0) - (int) 'A';
		int col = str.charAt(1) - (int) '0';

		if (str.length() > 2) {
			col = col * 10 + str.charAt(2) - (int) '0';
		}

		if (row < 0 || row > DIMENSION || col < 0 || col > DIMENSION) {
			throw new InvalidCoordinates("Outside the grid");
		}

		return new Pair<>(row, col);
	}

	@Override
	public String oceanGrid() {
		return this.ocean.toString();
	}

	@Override
	public String shotsGrid() {
		return this.shots.toString();
	}

	@Override
	public List<Integer> shipsToBePlaced() throws UnallowedAction {
		if (shipsLeft.isEmpty() && state == 0) {
			this.state = 1;
		}

		return shipsLeft;
	}

	@Override
	public void placeShip(String str)
			throws InvalidCoordinates, IncorrectlyPlacedShip, InvalidShipSize, UnallowedAction {
		if (this.state != 0) {
			throw new UnallowedAction("placeShip");
		}

		if (str == null || str.isEmpty() || !str.matches(PATTERN_COORD)) {
			throw new IllegalArgumentException(String.format("Coordinates pattern: %s", PATTERN_COORD));
		}

		Pattern pattern = Pattern.compile("\\p{Alpha}\\d{1,2}");
		Matcher matcher = pattern.matcher(str.subSequence(0, str.length()));
		matcher.find();
		String str0 = matcher.group();
		matcher.find();
		String str1 = matcher.group();

		Pair<Integer, Integer> p0 = position(str0);
		Pair<Integer, Integer> p1 = position(str1);

		Integer size = this.ocean.placeShip(p0, p1, this.shipsLeft);

		// a ship of a certain size has been placed
		this.shipsLeft.remove(size);
		shipsOnOcean.put(this.idClient, shipsOnOcean.get(this.idClient) + 1);
	}

	@Override
	public boolean startGame() throws UnallowedAction {
		if (this.state != 1) {
			throw new UnallowedAction("startGame" + this.state);
		}

		synchronized (mutex) {
			if (opponent.get(idClient) != null) {
				// the player has an opponent, but not the initial turn
				this.state = 2;
			} else {
				if (playersWaiting.isEmpty()) { // no opponent: wait for one
					playersWaiting.add(this.idClient);
				} else {
					if (playersWaiting.contains(idClient)) {
						if (playersWaiting.size() == 1) {
							// only this client in the queue: keep waiting for an opponent
							return false;
						}

						// a pairing can be made
						playersWaiting.remove((Integer) idClient);
					}

					// once ensured this player is not in the waiting list,
					// the first in the queue is assign as an opponent
					int idOpponent = playersWaiting.remove(0);
					opponent.put(idClient, idOpponent);
					opponent.put(idOpponent, idClient);

					mutexTurn.put(idClient, new Object());
					mutexTurn.put(idOpponent, mutexTurn.get(this.idClient));

					// the player that allows the pairing to be made
					// has the initial turn
					playerTurn.put(this.idClient, true);
					this.state = 2;
				}
			}
		}
		return this.state == 2;
	}

	@Override
	public int turn() throws UnallowedAction {
		if (this.state < 2) { // If the game hasn't started
			throw new UnallowedAction();
		}

		if (!opponent.containsKey(this.getOpponent()))
			return GAME_END;

		if (shipsOnOcean.get(this.idClient) == 0 || shipsOnOcean.get(this.getOpponent()) == 0)
			return GAME_END;

		if (this.state == 2) {
			if (playerTurn.get(this.idClient)) {
				this.state = 3;
			} else { // not the player's turn
				return 0;
			}
		}

		return 1;
	}

	@Override
	public String shotCoordinates(String shot) throws UnallowedAction, InvalidCoordinates {
		// If the game hasn't started or if it has ended or if it is not the player's
		// turn:
		if (this.state < 2 || this.state == GAME_END) {
			throw new UnallowedAction("shotcoordinates");
		}
		if (this.state == 2) {
			throw new UnallowedAction("Not your turn");
		}

		Pair<Integer, Integer> coords = position(shot);

		Cell hitOrMiss = this.ocean.shot(coords.first(), coords.second());

		if (!hitOrMiss.isShip()) {
			updateState(false);

			shots.table[coords.first()][coords.second()] = Cell.WATER;

			return "Miss";
		} else {
			updateState(true);

			Ship hit = (Ship) hitOrMiss;

			if (hit.equals(Ship.HIT)) {
				shots.table[coords.first()][coords.second()] = Ship.HIT;

				return "Hit";
			} else {
				shipsOnOcean.put(this.getOpponent(), shipsOnOcean.get(this.getOpponent()) - 1);
				shots.registerShip(hit);

				return "Sunk";
			}
		}
	}

	@Override
	public int numShipsOnOcean() {
		return shipsOnOcean.get(this.idClient);
	}

	/**
	 * Changes the state of the player's OOS regarding if their last shot was a hit
	 * or not, and switches turn if necessary. It is going to be used in
	 * shotCoordinates. It updates the state depending on the result of the shot
	 * (hit => true) and switches turn if necessary.
	 * 
	 * @param hit {@code true} if the shot hits an opponent's ship
	 */
	private void updateState(boolean hit) {
		if (this.state >= 5 || !hit) {
			this.state = 2;

			synchronized (mutexTurn.get(this.idClient)) {
				playerTurn.put(this.idClient, false);
				playerTurn.put(this.getOpponent(), true);
			}
		} else if (hit) {
			this.state++;
		}
	}

	private int getOpponent() {
		return opponent.get(this.idClient);
	}

	public void close() {
		if (this.state < 2) { // disconnection without having started a game
			synchronized (playersWaiting) {
				playersWaiting.remove((Integer) this.idClient);
			}
		}

		int idOpponent = opponent.get(this.idClient);
		if (opponent.get(idOpponent) != null) { // the opponent is still connected
			synchronized (mutexTurn.get(this.idClient)) {
				playerTurn.put(opponent.get(idOpponent), true);
			}
		} else { // the opponent is disconnected
			// delete the shared information of both players
			synchronized (playersOcean) {
				playersOcean.remove(this.idClient);
				playersOcean.remove(idOpponent);
			}
			synchronized (shipsOnOcean) {
				shipsOnOcean.remove(this.idClient);
				shipsOnOcean.remove(idOpponent);
			}
			synchronized (playerTurn) {
				playerTurn.remove(this.idClient);
				playerTurn.remove(idOpponent);
			}
			synchronized (mutexTurn) {
				mutexTurn.remove(this.idClient);
				mutexTurn.remove(idOpponent);
			}
		}

		// this player does not need to know who is the opponent anymore
		synchronized (opponent) {
			opponent.remove(this.idClient);
		}
		
		Battleship.super.close();
	}
}
