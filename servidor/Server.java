package servidor;

import java.io.IOException;

import lib.ChannelException;
import lib.CommServer;
import optional.Trace;

public class Server {
	public static void registerOperations(CommServer com) {
		com.addFunction("oceanGrid", (obj, x) -> ((Service)obj).oceanGrid());
		com.addFunction("shotsGrid", (obj, x) -> ((Service)obj).shotsGrid());
		
		com.addFunction("startGame", (obj, x) -> ((Service)obj).startGame());
		com.addFunction("turn", (obj, x) -> ((Service)obj).turn());
		com.addFunction("shotCoordinates", (obj, x) -> ((Service)obj).shotCoordinates((String)x[0]));
		com.addFunction("numShipsOnOcean", (obj, x) -> ((Service)obj).numShipsOnOcean());
		com.addFunction("shipsToBePlaced", (obj, x) -> ((Service)obj).shipsToBePlaced());
		
		com.addAction("placeShip", (obj, x) -> ((Service)obj).placeShip((String)x[0]));
	}

	public static void main(String[] args) {
		CommServer com;			// server's communication channel
		int idClient;			// client's identifier
		
		try {
			// 1. Create the communication channel
			com = new CommServer();
			
			// activate the trace in the server (it is not activated
			// by default). It is recommended to activate it to
			// show in the console the server's actions
			Trace.activateTrace(com);
			
			// activate the trace in the server (it is not activated
			// by default, optional operation)
			com.activateMessageLog();

			// 2. Register the service operations
			registerOperations(com);
			
			// 3. Leave the server waiting indefinitely 
			while (true) {
				// 3.1 Wait for a client (blocking operation) and identify it
				idClient = com.waitForClient();
				
				// 3.2 Launch the thread where the OOS will be created
		        // and the message exchange with the client will occur
				new Thread(new MyThreads(idClient, com)).start();				
			} // while
		} catch (IOException | ChannelException e) {
			// critical exceptions: the server is stopped
			System.err.printf("Error: %s\n", e.getMessage());
			e.printStackTrace();
		}
		
	} // main
}
