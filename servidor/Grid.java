package servidor;

import java.util.List;

import common.IncorrectlyPlacedShip;
import common.Battleship;
import common.InvalidShipSize;

/**
 * The instances of this class are ocean grids or shots grids.
 * A grid is a matrix of squares.
 */
class Grid {
	/**
	 * Matrix of squares that composes the grid.
	 */
	Cell[][] table;
	
	/**
	 * Dimension of a squared grid: {@code Battleship.DIMENSION}
	 */
	final static int DIM = Battleship.DIMENSION;

	/**
	 * Creates a DIM x DIM grid with no ships, all of its
	 * squares are {@code Square.WATER}
	 */
	Grid() {
		this.table = new Cell[DIM][DIM];
		for (int i = 0; i < DIM; i++) {
			for (int j = 0; j < DIM; j++) {
				this.table[i][j] = Cell.WATER;
			}
		}
	}
	
	/**
	 * Returns true if the specified row number and the column
	 * number is a valid position in this grid.
	 * @param row the row number
	 * @param col the column number
	 * @return true if the given values are non-negative and
	 * both lower than {@code DIMENSION}
	 */
	private boolean validPosition(int row, int col) {
		return row >= 0 && row < DIM && col >= 0 && col < DIM;
	}
	
	/**
	 * Returns true if the specified row number and the column
	 * number is a valid position in this grid.
	 * @param p coordinates (row, column)
	 * @return true if the coordinates are valid
	 */
	private boolean validPosition(Pair<Integer, Integer> p) {
		return validPosition(p.first(), p.second());
	}
	
	/**
	 * Checks if the specified placement for this ship is valid.
	 * @param p0 coordinates of the lower edge of the ship
	 * @param p1 coordinates of the upper edge of the ship
	 * @throws IncorrectlyPlacedShip
	 */
	private void checkShipPosition(Pair<Integer, Integer> p0,
			Pair<Integer, Integer> p1) throws IncorrectlyPlacedShip {
		// since the ships cannot touch each other, we also have to check
		// if the squares surrounding the ship are water, except for the
		// sides which are at the edge of the grid
		int row0 = p0.first() > 0 ? p0.first() - 1 : p0.first();
		int row1 = p1.first() < DIM - 1 ? p1.first() + 1 : p1.first();
		int col0 = p0.second() > 0 ? p0.second() - 1 : p0.second();
		int col1 = p1.second() < DIM - 1 ? p1.second() + 1 : p1.second();
		// the region: row0, row1, col0 and col1, must be water
		for (int i = row0; i <= row1; i++) {
			for (int j = col0; j <= col1; j++) {
				if (this.table[i][j] != Cell.WATER) {
					throw new IncorrectlyPlacedShip(
							String.format("Error in position %c%d: %s",
									(char)((int)'A' + i), j,
									this.table[i][j].isShip() ? "there is a ship"
											                   : "separation between ships is required"));
				}
			}
		}
	}
	
	/**
	 * Checks if p0 and p1 are the lower and upper edges of the ship,
	 * respectively. If not, switches the coordinates.
	 * @param p0 an edge of the ship
	 * @param p1 another edge of the ship
	 * @param vertical {@code true} if the ship is placed vertically
	 */
	private void checkEdges(Pair<Integer, Integer> p0,
			Pair<Integer, Integer> p1, boolean vertical) {
		Pair<Integer, Integer> temp = new Pair<>(p0);
		if (vertical) { // same column
			// p0 lower row point and p1 upper row point
			if (p0.first() > p1.first()) { // switch
				p0.setFirst(p1.first());
				p1.setFirst(temp.first());
			}			
		} else { // same row
			// p0 lower column point and p1 upper column point
			if (p0.second() > p1.second()) { // switch
				p0.setSecond(p1.second());
				p1.setSecond(temp.second());
			}			
		}
	}

	/**
	 * Place a ship between the specified initial and final positions of this grid.
	 * @param p0 the initial position, pair (row, col), of the ship
	 * @param p1 the final position, pair (row, col), of the ship
	 * @param sizes ships left
	 * @return the size of the ship placed in the grid
	 * @throws IncorrectlyPlacedShip if the ship is placed on a square that is not
	 * water, do not leave water surrounding or does not have the required size 
	 * @throws InvalidShipSize if the size of the ship does not match one of the
	 * ships left
	 */
	int placeShip(Pair<Integer, Integer> p0,
			Pair<Integer, Integer> p1, List<Integer> sizes)
					throws IncorrectlyPlacedShip, InvalidShipSize {
		if (!validPosition(p0) || !validPosition(p1)) {
			throw new IncorrectlyPlacedShip("Placement out of the grid.");
		}
		int sizeV = Math.abs(p1.first()- p0.first())  + 1;
		int sizeH = Math.abs(p1.second() - p0.second()) + 1;
		
		// check if the ship is in horizontal or vertical position
		if (sizeV != 1 && sizeH != 1) {
			throw new IncorrectlyPlacedShip(
					"Place the ship parallel to an edge of the grid of width 1.");
		}
		
		// ship size
		int size = Math.max(sizeH, sizeV);
		
		// check if the size is valid
		if (!sizes.contains(size)) {
			throw new InvalidShipSize(String.format(
					"Valid sizes: %s", sizes.toString()));
		}
		
		// check if (or make) p0 and p1 are the lower and upper
		// edges of the ships, respectively
		checkEdges(p0, p1, sizeV >= sizeH);
				
		// check that everything is water in the position where
		// the ship is placed and the surroundings
		checkShipPosition(p0, p1);
		
		// create the ship of the given size and place it on the grid
		Ship b = new Ship(this, p0, p1, size);
		int row = p0.first();
		int col = p0.second();
		
		// square that the ship occupies on the grid
		if (sizeV >= sizeH) { // vertical ship, the row varies
			for (int i = 0; i < sizeV; i++) {
				this.table[row + i][col] = b;
			}
			return sizeV;
		} else { // horizontal ship, the column varies
			for (int j = 0; j < sizeH; j++) {
				this.table[row][col + j] = b;
			}
			return sizeH;
		}

	}
	
	/**
	 * Changes in this grid the water in the region: p0.first(),
	 * p1.first(), p0.second() and p1.second() and surroundings to
	 * separation between ships.
	 * <p>Useful in ocean grids and shots grids, for marking the
	 * squares where the should be no ships.</p>
	 * @param p0 lower coordinates of a sunk ship
	 * @param p1 upper coordinates of a sunk ship
	 */
	void setSeparation(Pair<Integer, Integer> p0,
			Pair<Integer, Integer> p1) {
		int row0 = p0.first() > 0 ? p0.first() - 1 : p0.first();
		int row1 = p1.first() < DIM - 1 ? p1.first() + 1 : p1.first();
		int col0 = p0.second() > 0 ? p0.second() - 1 : p0.second();
		int col1 = p1.second() < DIM - 1 ? p1.second() + 1 : p1.second();
		for (int i = row0; i <= row1; i++) {
			for (int j = col0; j <= col1; j++) {
				if (this.table[i][j] == Cell.WATER) {
					this.table[i][j] = Cell.SEPARATOR;
				}
			}
		}
	}

	/**
	 * Sets the sunk ship in this grid. The grid must be a shots grid.
	 * @param s the sunk ship to be registered
	 */
	void registerShip(Ship s) {
		Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> ext = s.edges();
		Pair<Integer, Integer> p0 = ext.first();
		Pair<Integer, Integer> p1 = ext.second();
		
		// set separation in the ship surroundings
		setSeparation(p0, p1);
		
		// place the ship
		int sizeV = Math.abs(p1.first()- p0.first())  + 1;
		int sizeH = Math.abs(p1.second() - p0.second()) + 1;		
		int row = p0.first();
		int col = p0.second();
		
		// squares that it occupies on the grid
		if (sizeV >= sizeH) { // vertical ship, the row varies
			for (int i = 0; i < sizeV; i++) {
				table[row + i][col] = s;
			}
		} else { // horizontal ship, the column varies
			for (int j = 0; j < sizeH; j++) {
				table[row][col + j] = s;
			}
		}
	}
	
	/**
	 * If the square {@code s} in the given position of this grid is a ship,
	 * returns {@code c.hit(row, col)}, otherwise returns {@code s} and
	 * leaves this square as {@code Square.MISS}.
	 * @param row the square's row
	 * @param col the square's column
	 * @return {@code c.hit(row, col)} if the square {@code s} with
	 * coordinates {@code (row, col)} of this grid is a ship and
	 * {@code Square.MISS} otherwise
	 */
	Cell shot(int row, int col) {
		if (!this.table[row][col].isShip()) {
			Cell s = this.table[row][col];
			// register the shot on the opponent's ocean and
			// returns the current state
			this.table[row][col] = Cell.MISS;
			return s;
		}
		// a ship has been hit
		return this.table[row][col].hit(row, col);
	}
	
	/**
	 * Returns the representation as characters of the ocean
	 * grid or the shots grid of a player.
	 * @return the string representing the player's grid
	 */
	@Override
	public String toString() {
		StringBuilder out = new StringBuilder("     ");
		StringBuilder line = new StringBuilder("   +");
		
		// numbers from 0 to DIM-1
		for (int k = 0; k < DIM; k++) {
			if (k < 10) {
				out.append(String.format("%d   ", k));
			} else {
				out.append(String.format("%d  ", k));
			}
			line.append("---+");
		}
		out.append('\n');
		line.append('\n');
		
		// table
		char ch = 'A';
		out.append(line);
		for (int i = 0; i < DIM; i++) {
			out.append(String.format(" %c |", ch++));
			for (int j = 0; j < DIM; j++) {
				if (this.table[i][j].isShip()) {
					out.append(String.format(
							" %s |", this.table[i][j].toString(i, j)));
				} else {
					out.append(String.format(
							" %s |", this.table[i][j].toString()));
				}
			}
			
			out.append('\n');
			out.append(line);
		}
		
		return out.toString();
	}
		
} // class Grid
