package servidor;

import java.io.IOException;

import lib.ChannelException;
import lib.CommServer;
import lib.ProtocolMessages;

public class MyThreads implements Runnable {
	private int idClient;				// Client's ID
	private CommServer com;				// Server's communication channel
	
	/**
	 * Create an execution thread for the OOS of the identified client
	 * as specified.
	 * @param idClient the client's ID
	 * @param com the server's communication channel
	 */
	public MyThreads(int idClient, CommServer com) {
		this.idClient = idClient;
		this.com = com;
	}

	/**
	 * Execute the thread corresponding to a client. Creates the OOS
	 * for this client and allows the conversation with the client
	 * (exchanges messages) until it is disconnected.
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		Service serviceObj = null;	// object of operations of the service (OOS)
		ProtocolMessages request;	    // income message (client's request)
		ProtocolMessages response;		// outcome message (response for the client)

		try {
			// 1. Create the OOS in the thread for a client
			serviceObj = new Service(idClient);

			// 2. Exchange messages with that client
			while (!com.closed(idClient)) {
				try {
		    		// 2.1 Wait for the client's serialized order
					request = com.waitEvent(idClient);
					
					// 2.2 Evaluate the received order
		    		response = com.processEvent(idClient, serviceObj,
		    				request);
					
		    		if (response != null) { // operation with a response
		    			// 2.3 Send the response to the client
		    			com.sendReply(idClient, response);
	    			}
				}
				catch (ClassNotFoundException e) {
					// Not critical exception (a message is lost)
					System.err.printf(
							"Error in the client's request %d: %s\n",
							idClient, e.getMessage());
				}
			}
		} catch (IOException | ChannelException e) {
			System.err.printf("Error: %s\n", e.getMessage());
		} finally {
			// 3. Close the service object when the client has disconnected
			if (serviceObj != null) {
				serviceObj.close();
			}
		}
	}

}
