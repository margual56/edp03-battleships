package servidor;

import java.util.HashSet;
import java.util.Set;

/**
 * The instances of this class are the ships of a grid. A ship is a specific square
 * of the grid that has a size: the number of rows and columns that it occupies
 * (expanded or combined square)
 */
class Ship extends Cell {
	/**
	 * Constant that indicates the representation of the squares
	 * of a ship as a string.
	 */
	final static Ship SHIP = new Ship('B');
	/**
	 * Constant that indicates the representation of the square
	 * of a hit ship, but not sunk, as a string.
	 */
	final static Ship HIT = new Ship('T');
	/**
	 * Constant that indicates the representation of the square
	 * of a sunk ship as a string.
	 */
	final static Ship SUNK = new Ship('X');
	
	private Set<Pair<Integer, Integer>> targets;	// coordinates of the already hit squares of the ship
	private Grid ocean;								// ocean where the ship is placed
	private Pair<Integer, Integer> low;				// position of the lower edge of the ship on the ocean
	private Pair<Integer, Integer> up;				// position of the upper edge of the ship on the ocean
	private int size;								// size of the ship
	
	private Ship(char ch) {
		super(ch);
	}

	/**
	 * Creates an expanded ship square between the given coordinates for
	 * the specified grid. Valid coordinates are required and the expanded
	 * square must have the specified size.
	 * @param gr the grid
	 * @param p0 the coordinates of the lower occupied square
	 * @param p1 the coordinates of the upper occupied square
	 * @param size the number of occupied squares
	 */
	Ship(Grid gr, Pair<Integer, Integer> p0,
			Pair<Integer, Integer> p1, int size) {
		super(SHIP.toString().charAt(0));
		this.targets = new HashSet<>();
		this.ocean = gr;
		this.low = new Pair<>(p0);
		this.up = new Pair<>(p1);
		this.size = size;
	}
		
	/**
	 * Returns {@code true}
	 * @return {@code true}
	 */
	@Override
	boolean isShip() {
		return true;
	}
	
	/**
	 * The pair of edges, lower and upper, of this ship on the ocean.
	 * @return the placement of this ship on the ocean
	 */
	@Override
	Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> edges() {
		return new Pair<>(this.low, this.up);
	}
	
	/**
	 * The string with the character with which the ship square in the given coordinates
	 * is represented.
	 * <p>In general, this string is the representation of the given square as a string
	 * of characters {@code toString()}. But since a ship, which is also a type of Square,
	 * usually occupies several squares of a grid, this string can only change (from
	 * {@code SHIP.toString()} to {@code SUNK.toString()} when it is sunk, not being possible
	 * to establish a different representation ({@code HIT.toString()} for any of the squares
	 * that the ship occupies.</p>
	 * @param row the row number of the ship square on the grid
	 * @param col the column number of the ship square on the grid
	 * @return {@code SHIP.toString()} if the ship is not sunk and the square in the given
	 * position has not been hit by an opponent's shot, {@code SUNK.toString()} if the ship
	 * that occupies the given square is sunk and {@code HIT.toString()} if the ship is not
	 * sunk, but the square in the given position has been hit by an opponent's shot
	 */
	@Override
	String toString(int row, int col) {
		if (this == HIT || this.targets.isEmpty() || this.targets.size() == size) {
			return toString();
		}
		
		Pair<Integer, Integer> coord = new Pair<>(row, col);		
		return this.targets.contains(coord) ? HIT.toString() : toString();
	}
	
	/**
	 * Returns {@code HIT} or this ship in case it is sunk. In this last case, it also
	 * changes its representation to {@code SUNK.toString()} and the representation of
	 * every surrounding square to {@code Square.SEPARATOR}.
	 * @return {@code HIT} or {@code this} if the ship is sunk. In case of shooting
	 * a square of an already sunk ship, it returns {@code SUNK}
	 */
	@Override
	Cell hit(int row, int col) {
		if (this.targets.size() == this.size) {
			return SUNK;
		}
		
		if (this.targets.add(new Pair<>(row, col))) {
			if (this.targets.size() == size) {
				// sets separating water on the surroundings
				ocean.setSeparation(this.low, this.up);
				// mark the representation of the ship as sunk
				this.ch = SUNK.toString().charAt(0);
				return this; // returns the sunk ship to register it in the shots grid
			}
		}

		return HIT;
	}	
}
