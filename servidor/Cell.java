package servidor;

/**
 * The instances of this class are the squares of the grids. Every
 * square is represented as a string through a specific character.
 */
class Cell {
	/**
	 * Constant that indicates the representation of the squares
	 * that are water.
	 */
	final static Cell WATER = new Cell(' ');
	/**
	 * Constant that indicates the representation of the squares
	 * that are water surrounding a ship.
	 */
	final static Cell SEPARATOR = new Cell('.');
	/**
	 * Constant that indicates the representation of the squares
	 * that are water but have been hit by a shot.
	 */
	final static Cell MISS =  new Cell('•');
	
	/**
	 * Character with which a square is represented.
	 */
	protected char ch;

	/**
	 * Creates a square represented by the specified character.
	 * @param ch the character
	 */
	Cell(char ch) {
		this.ch = ch;
	}
	
	/**
	 * Returns {@code false}
	 * @return {@code false}
	 */
	boolean isShip() {
		return false;
	}
	
	/**
	 * @return the edges of an expanded square (a ship)
	 * @throws UnsupportedOperationException if the square is not a ship
	 */
	Pair<Pair<Integer, Integer>, Pair<Integer, Integer>> edges() {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param row the row of the square in the grid
	 * @param col the column of the square in the grid
	 * @return the string with which the square in the given coordinates
	 * is represented and which is part of an expanded square (ship)
	 * @throws UnsupportedOperationException if the square is not a ship
	 */
	String toString(int row, int col) {
		throw new UnsupportedOperationException();
	}
	
	/**
	 * @param row the row of the square in the grid
	 * @param col the column of the square in the grid
	 * @return the square in the given position
	 * @throws UnsupportedOperationException if the square is not a ship
	 */
	Cell hit(int row, int col) {
		throw new UnsupportedOperationException();
	}

	@Override
	public String toString() {
		return String.format("%c", this.ch);
	}
	
}
