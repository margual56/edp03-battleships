package servidor;

import java.util.Objects;

/**
 * Data type of pair of elements.
 *
 * @param <K> the comparable type for the first component
 * @param <V> the comparable type for the second component
 */
public class Pair<K extends Comparable<K>, V extends Comparable<V>> 
	implements Comparable<Pair<K, V>> {
	private K first;	// first component of the pair
	private V second;	// second component of the pair
	
	/**
	 * Creates a pair with the specified elements.
	 * @param first first component of the pair
	 * @param second second component of the pair
	 */
	public Pair(K first, V second) {
		this.first = first;
		this.second = second;
	}
	
	/**
	 * Creates a copy of the specified pair.
	 * @param p the pair to be copied
	 */
	public Pair(Pair<K, V> p) {
		this.first = p.first();
		this.second = p.second();
	}
	
	/**
	 * Returns the first component of the pair.
	 * @return the first component of the pair
	 */
	public K first() {
		return first;
	}
	
	/**
	 * Returns the second component of the pair.
	 * @return the second component of the pair
	 */
	public V second() {
		return second;
	}
	
	/**
	 * Modifies the first component of the pair with
	 * the specified value.
	 * @param k the new first component of the pair
	 */
	public void setFirst(K k) {
		first = k;
	}
	
	/**
	 * Modifies the second component of the pair with
	 * the specified value.
	 * @param k the new second component of the pair
	 */
	public void setSecond(V v) {
		second = v;
	}
	
	/**
	 * Returns the representation of the pair as a string.
	 * @return a string of characters representing the pair
	 */
	@Override
	public String toString() {
		return String.format("(%s, %s)", first().toString(), second.toString());
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(first, second);
	}

	@SuppressWarnings("unchecked")
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Pair<K, V> other = (Pair<K, V>) obj;
		return Objects.equals(first, other.first) && Objects.equals(second, other.second);
	}

	@Override
	public int compareTo(Pair<K, V> o) {
		int x = this.first().compareTo(o.first());
		
		if (x == 0) {
			return this.second().compareTo(o.second());
		}
		
		return x;
	}
	
}
